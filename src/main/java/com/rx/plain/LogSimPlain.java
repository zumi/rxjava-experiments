package com.rx.plain;

import java.util.concurrent.Callable;

public class LogSimPlain implements Callable<Void> {
	
	@Override
	public Void call() throws Exception {
		for (int i = 0; i < 10000; i++) {
			try {
				Thread.sleep(0);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

}
