package com.rx.vertx;

import java.util.concurrent.Callable;

import org.vertx.java.core.eventbus.EventBus;

public class LogSimEventBus implements Callable<Void> {

	private EventBus eventBus;

	public LogSimEventBus(EventBus eventBus) {
		this.eventBus = eventBus;
	}

	@Override
	public Void call() throws Exception {
		for (int i = 0; i < 10000; i++) {
			eventBus.publish("myLogStream", "Vertx " + i);
			try {
				Thread.sleep(0);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

}
