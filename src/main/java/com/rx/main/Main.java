package com.rx.main;

import java.time.Duration;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.vertx.java.core.Handler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.VertxFactory;
import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.core.eventbus.Message;

import com.rx.plain.LogSimPlain;
import com.rx.reactive.LogSimReactive;
import com.rx.sout.LogSimSout;
import com.rx.vertx.LogSimEventBus;

import rx.Observable;
import rx.Observable.OnSubscribe;
import rx.functions.Action1;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.observables.ConnectableObservable;
import rx.observers.Observers;
import rx.schedulers.Schedulers;

public class Main {

	public static void main(String... args) throws InterruptedException, ExecutionException {

		Vertx vertx = VertxFactory.newVertx();
		EventBus eventBus = vertx.eventBus().registerHandler("myLogStream", new Handler<Message>() {

			@Override
			public void handle(Message message) {
				System.out.println(message.body().toString());

			}
		});

		Observer<Integer> soutObserver = new Observer<Integer>() {

			@Override
			public void onNext(Integer t) {
				System.out.println("Rx "+t);

			}

			@Override
			public void onError(Throwable e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onCompleted() {
				System.out.println("completed");

			}
		};

		ExecutorService executorService = Executors.newFixedThreadPool(10);

		ArrayList<Future<Void>> futureList = new ArrayList<>();
		long startTime1 = System.currentTimeMillis();
		for (int i = 0; i < 10; i++) {
			futureList.add(executorService.submit(new LogSimPlain()));
		}
		for (Future<Void> future : futureList) {
			future.get();
		}
		Duration durationPlain = Duration.ofMillis(System.currentTimeMillis() - startTime1);
		futureList.clear();

		futureList = new ArrayList<>();
		startTime1 = System.currentTimeMillis();
		for (int i = 0; i < 10; i++) {
//			futureList.add(executorService.submit(new LogSimSout()));
		}
		for (Future<Void> future : futureList) {
			future.get();
		}
		Duration durationSout = Duration.ofMillis(System.currentTimeMillis() - startTime1);
		futureList.clear();

		futureList = new ArrayList<>();
		startTime1 = System.currentTimeMillis();
		for (int i = 0; i < 10; i++) {
			LogSimReactive logSim = new LogSimReactive();
			logSim.subscribeTo().subscribe(soutObserver);
			futureList.add(executorService.submit(logSim));
		}
		
		for (Future<Void> future : futureList) {
			future.get();
		}
		Duration durationRx = Duration.ofMillis(System.currentTimeMillis() - startTime1);
		
		futureList = new ArrayList<>();
		startTime1 = System.currentTimeMillis();
		for (int i = 0; i < 10; i++) {
//			futureList.add(executorService.submit(new LogSimEventBus(eventBus)));
		}
		for (Future<Void> future : futureList) {
			future.get();
		}
		Duration durationVertx = Duration.ofMillis(System.currentTimeMillis() - startTime1);
		futureList.clear();
		
		
		executorService.shutdown();
//		futureList.clear();

//		Thread.sleep(2000);

//		executorService.shutdownNow();

		// ConnectableObservable<Integer> test = Observable.create(new
		// OnSubscribe<Integer>() {
		//
		// @Override
		// public void call(Subscriber<? super Integer> t) {
		// for (int i = 0; i < 10000; i++) {
		// try {
		// Thread.sleep(0);
		// } catch (InterruptedException e) {
		// e.printStackTrace();
		// }
		// t.onNext(i);
		// }
		//
		// }
		// }).publish();
		//
		// Observer<Integer> soutObserver = new Observer<Integer>() {
		//
		// @Override
		// public void onNext(Integer t) {
		// System.out.println(t);
		//
		// }
		//
		// @Override
		// public void onError(Throwable e) {
		// // TODO Auto-generated method stub
		//
		// }
		//
		// @Override
		// public void onCompleted() {
		// System.out.println("completed");
		//
		// }
		// };

		// ExecutorService mys = Executors.newFixedThreadPool(10);
		// for (int i = 0; i < 10; i++) {
		// test.observeOn(Schedulers.from(mys)).subscribe(soutObserver);
		// }
		//
		// long startTime2 = System.currentTimeMillis();
		// test.connect();
		// Duration durationRx = Duration.ofMillis(System.currentTimeMillis() -
		// startTime2);
		 Thread.sleep(2000);
		// System.out.println("Rx: "+durationRx);
		System.out.println("Plain: " + durationPlain);
		System.out.println("Sout: " + durationSout);
		System.out.println("Vertx: " + durationVertx);
		System.out.println("Rx: " + durationRx);

	}

	// System.out.println(Observable.range(0,
	// 10).count().toBlocking().single());
	//
	// Observable<Integer> odds = Observable.create(new
	// Observable.OnSubscribe<Integer>() {
	//
	// @Override
	// public void call(Subscriber<? super Integer> sub) {
	// ExecutorService service = Executors.newFixedThreadPool(5);
	// for (int i = 1; i < 10; i = i + 2) {
	// service.submit(new MyCallable(i, sub));
	// }
	//
	// }
	// });
	//
	// Observable<Integer> evens = Observable.create(new
	// Observable.OnSubscribe<Integer>() {
	//
	// @Override
	// public void call(Subscriber<? super Integer> sub) {
	// ExecutorService service = Executors.newFixedThreadPool(5);
	// for (int i = 0; i < 10; i = i + 2) {
	// service.submit(new MyCallable(i, sub));
	// }
	//
	// }
	// });
	//
	// Observable.merge(odds, evens).subscribe(new Subscriber<Integer>() {
	//
	// public void onCompleted() {
	// // TODO Auto-generated method stub
	//
	// }
	//
	// public void onError(Throwable e) {
	// // TODO Auto-generated method stub
	//
	// }
	//
	// public void onNext(Integer t) {
	// System.out.println(t);
	// }
	// });
	// }
	// }
	//
	// class MyCallable implements Callable<Void> {
	//
	// int i = 0;
	// Subscriber<? super Integer> sub;
	//
	// public MyCallable(int i, Subscriber<? super Integer> sub) {
	// this.i = i;
	// this.sub = sub;
	// }
	//
	// @Override
	// public Void call() throws Exception {
	// sub.onNext(i);
	// return null;
	// }

}
