package com.rx.main;

import java.time.Duration;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.rx.reactive.LogSimReactive;

import rx.Observable;
import rx.Observer;

public class RxMain {
	public static void main(String... args) throws InterruptedException, ExecutionException {

		Observer<Integer> soutObserver = new Observer<Integer>() {

			@Override
			public void onNext(Integer t) {
				System.out.println("Rx " + t);

			}

			@Override
			public void onError(Throwable e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onCompleted() {
				System.out.println("completed");

			}
		};

		ExecutorService executorService = Executors.newFixedThreadPool(10);

		ArrayList<Future<Void>> futureList = new ArrayList<>();
		long startTime1 = System.currentTimeMillis();
		LogSimReactive logSim = new LogSimReactive();
		
		// count all events
//		logSim.subscribeTo().count().subscribe(soutObserver);
		
		// take a sample every x seconds
//		logSim.subscribeTo().sample(100, TimeUnit.MILLISECONDS).subscribe(soutObserver);
		
		// add (the values) and returns the result
//		logSim.subscribeTo().scan(new Func2<Integer, Integer, Integer>() {
//
//			@Override
//			public Integer call(Integer t1, Integer t2) {
//				return t1 + t2;
//			}
//			
//		}).subscribe(soutObserver);
		
		// window
		logSim.subscribeTo().window(50, TimeUnit.MILLISECONDS).subscribe(new Observer<Observable<Integer>>() {

			@Override
			public void onCompleted() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onError(Throwable e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onNext(Observable<Integer> t) {
				t.count().subscribe(soutObserver);
			}
		});
		
		futureList.add(executorService.submit(logSim));
		
		
		for (Future<Void> future : futureList) {
			future.get();
		}
		Duration durationRx = Duration.ofMillis(System.currentTimeMillis() - startTime1);

		executorService.shutdown();

		System.out.println(durationRx);

	}

}