package com.rx.sout;

import java.util.concurrent.Callable;

public class LogSimSout implements Callable<Void> {

	@Override
	public Void call() throws Exception {
		for (int i = 0; i < 10000; i++) {
			System.out.println(i);
			try {
				Thread.sleep(0);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

}
