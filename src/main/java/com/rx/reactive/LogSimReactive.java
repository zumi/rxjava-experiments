package com.rx.reactive;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import rx.Observable;
import rx.schedulers.Schedulers;
import rx.subjects.ReplaySubject;

public class LogSimReactive implements Callable<Void> {
	
	private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(5);
	
	ReplaySubject<Integer> subject = ReplaySubject.create();
	
	@Override
	public Void call() throws Exception {
		for (int i = 0; i < 10000; i++) {
			// log
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			// notify
			subject.onNext(i);
		}
		
		subject.onCompleted();
		return null;
	}
	
	public Observable<Integer> subscribeTo(){
		return subject.subscribeOn(Schedulers.io());
	}

}